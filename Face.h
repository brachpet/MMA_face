#pragma once
#include "GL/glew.h"
#include <glm/glm.hpp>

#include "ShaderProgram.h"

enum faceExpressions {
	EXPRESSION_HAPPY = 0,
	EXPRESSION_SAD,
	EXPRESSION_NEUTRAL
};

class Face {
	int numOfFaces;
	int numOfVertecesInOneFace;
	int numOfFloatsInOneVertex;

	ShaderProgram shaderProgram;

	GLuint vertexArrayID;	///vertexBuffer ID
	GLuint vertexBuffer;	///vertexBuffer
	float* vertexData;		///vertex data vsech obliceju seriove za sebou

	float t;

	float* assimpMesh;
	GLuint assimpVertexBufferObject;
	GLuint assimpVertexArrayObject;
	GLuint assimpElementArrayObject;
	GLuint assimpVertexArrayID;
	int assimpNumTriangles;
	int assimpStride;
	GLuint tex;

	glm::mat4 * boneTransformations;

	GLuint w_transformLocation[19];

	void loadMesh();

	void loadTextures();

	int selectedBone = 0;

public:
	bool showBone = true;
	bool transformCalcStarted = false;

	int numberOfTotalFaces = 0;
	int faceNumber = 0;

	Face();
	~Face();

	void render();
	
	void selectBone(int index);
	void selectNextBone();
	void selectPrevBone();
	void saveActualFace();
	void loadFaces(string filename);
	void resetFace();
	void switchFace(bool random, float dt);
	void setFaceNumber(int number);

	int getFaceNumber();

	void setTransformOfSelectedBone(float * translationGoal, float dt);
	void setRotationOfSelectedBone(float * quatGoal, float dt);
	void setTransformOfSelectedBoneFromGui(float * translationGoal);
	void setRotationOfSelectedBoneFromGui(float * quatGoal);
	void resetTransform(float* translation);
	void resetRotation(float* quat);

	float approach(float goal, float current, float dt);

	glm::vec3 getTransformOfSelectedBone();
	glm::vec4 getRotationOfSelectedBone();
	glm::vec3 getTransformOfSelectedBoneGoal();
	glm::vec4 getRotationOfSelectedBoneGoal();

};


class Bone {
public:
	glm::mat4 spaceTransform;
	glm::mat4 spaceTransformInverse;
	glm::mat4 localTransform;

	glm::vec3 position;
	glm::vec4 rotation;
	glm::mat4 rotationMat;

	glm::mat4 getLocalTransform();
	void setPosition(glm::vec3 position);
	void setRotation(glm::mat4 rotation);
};


static const GLfloat testingTriangleData1[] = {
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	0.0f,  1.0f, 0.0f,
};

static const GLfloat testingTriangleData2[] = {
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	0.0f,  0.5f, 0.0f,
};

