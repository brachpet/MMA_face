\documentclass[11pt]{article}

\usepackage[czech]{babel}
\usepackage[utf8]{inputenc}
\usepackage{cite}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}


\title{MMA, simulace lidské tváře}
\author{Petr Brachaczek, Sebastian Voráč}


\begin{document}
\maketitle

\section{Zadání}
Zrealizujte implementaci simulace mimiky pomocí modelu tváře. Zobrazení bude umožňovat full-screen režim, rozlišení, nastavení kamery a textury na~obličeji a~dále nastavení výrazu v obličeji a~jeho animaci.

\section{Model hlavy}
Z důvodu originality jsme se rozhodli rozpohybovat tvář neživé sochy. Toto rozhodnutí také mnohem lépe zapadalo do kompozičního kontextu společné prezentace.

Jako model byla zvolena mramorová busta muže s věncem. Díky světlé barvě, absenci vousů a výrazným rysům má tato socha veškeré předpoklady pro naše využití. 

Z důvodu použití takového modelu nebylo možné zcela vyhovět požadavku zadání na volitelnost textury na obličeji. Texturu modelu hlavy je sice možné změnit, ovšem není zcela triviální zařídit, aby byl výsledek vizuálně kvalitní.

\subsection{Geometrie a textury}
Základem se stal 3D scan, jehož autorem je Geoffrey Marchal, který jej poskytl volně ke stažení \cite{busta}.

3D scan jako takový je však nevhodný pro zobrazování v reálném čase. Rovněž postrádá vlastnosti pro dobrou realizaci obličejového rigu. Z tohoto důvodu bylo potřeba vytvořit verzi modelu s mnohem menším počtem polygonů. Bylo taky potřeba zajistit, aby měl model přívětivě rozloženou topologii polygonů vhodnou pro lidskou tvář. Důležité jsou v tomto ohledu hlavně smyčky kolem úst, očí, brady, špičky nosu a podobně. Vhodná topologie napomáhá k vizuální korektnosti mimických pohybů.

Retopologie modelu byla provedena pomocí nástroje 3Ds max 2017. Textury scanu byly následně přeneseny na nový model pomocí projekčního promítání. Díky tomu byl snížen počet polygonů z původních 446~234 na~23~766 a díky dobrému mapování jsem byl schopen použít texturu 2048x2048 pixelů oproti původním 8192x8192 pixelů.

\begin{figure}[H]
\includegraphics[width=1\textwidth]{highLowPoly.png}
\caption{Původní a přepracovaná topologie modelu.}
\label{wireframe}
\end{figure}

\begin{figure}[H]
\includegraphics[width=1\textwidth]{highLowPolySolid.png}
\caption{Vizuální rozdíl původního a upraveného modelu je minimální}
\label{noDifference}
\end{figure}

\subsection{Obličejový rig}
Dobrý rig je základem kvalitní animace. Obličejové rigy jsou jedny z těch komplexnějších. Příkladem jest například velmi pokročilý rig studia Snappers, které realizovalo podobnou simulaci v Unreal Enginu \cite{snappers}. Takto detailní řešení však nebylo možné realizovat z důvodu nedostatku zkušeností a omezenému času. Kvůli tomu také nebylo možné provést více vývojových iterací rigu.

Řešením bylo využít mnohem menší počet řídících bodů. Omezujícím se stal také fakt, že nepracujeme s modelem lidské hlavy, ale sochou. Určitá gesta, jako například úplné zavírání očí či otevírání úst, nebylo možné reprodukovat, takže pro ně nebylo potřeba vytvářet řídící body.

Celkem bylo využito 19 řídících bodů, z nichž jeden byl statický. Aby bylo možné řídící body zrcadlit, byl model posunut a natočen tak, aby byl přibližně souměrný podle svislou roviny. Mírným problémem byla nedokonalá symetrie samotného modelu. Z tohoto důvodu nebylo možné rigovat pouze polovinu obličeje a druhou polovinu rigu zkopírovat.

Rozmístění řídících bodů bylo zvoleno tak, aby umožňovalo reprodukovat jednoduché výrazy. Aby bylo možné semestrální práci zpracovat, zaměřil jsem se na nejvýraznější mimické oblasti. Z toho vyplývá, že náš nástroj nebude tak dokonalý, aby dokázal reprodukovat jemné detaily.

Zaměřil jsem se na výrazové oblasti a umístil řídící body, aby bylo možné řídit:
\begin{itemize}
\item úhel, výšku a zaoblení obočí,
\item mírné přivírání očí,
\item výšku brady,
\item šířku a zaoblení úst,
\item pnutí v lícních částech,
\item rozšíření nosních dírek.
\end{itemize}

Rig byl realizován v nástroji 3Ds max 2017.

\begin{figure}[H]
\includegraphics[width=1\textwidth]{rig.png}
\caption{Rozmístění řídících bodů a jejich oblasti vlivu.}
\label{rig}
\end{figure}


\section{Aplikace}
Aplikace je realizovaná v jazyce C++ s vykreslováním na grafické kartě pomocí standardu OpenGL.

\subsection{Import a zpracování modelu}
Model busty včetně rigu byl do aplikace importován za pomocí knihovny Assimp. Ta zásadním způsobem zjednodušila celý proces, protože dokáže zpracovat v plném rozsahu formát FBX, který jako jeden z mála dokáže uchovat informace o řídících bodech a jejich oblastech vlivu v polygonální síti.

Model je renderován pomocí indexované sítě, která napomáhá cachování výstupu z vertex shaderu, který je v našem případě nejsložitější z celého shader programu. Díky tomu bylo dosaženo rychlejšího vykreslování.

Každý vrchol v modelu si nese informaci o své pozici, normále a mapovací UV souřadnici. Dále si pak nese všech 19 vah k řídím bodům (ke každému řídícímu bodu jednu).

Každý řídící bod si v aplikaci nese svou transformační matici do prostoru bodu, její inverzi a dále pak lokální transformační data (posun, rotace), které budou v režii uživatele.

\subsection{Rendering deformovatelného obličeje}
Model obličeje vstupuje do zobrazovacího řetězce ve své původní podobě tak, jak je reprezentován v bufferu vrcholů. Ve vertex bufferu je pak na každý vrchol aplikována příslušná transformace. V našem případě však ne pouze transformace kamery a perspektivní projekce, ale také vážené transformace vzhledem ke všem kostem.

Do vertex bufferu vstupují jako uniform proměnná pole všech 19 transformací k příslušným kostem. Ta vznikne složením následujících transformací:
$$T = T_{space}^{-1} \cdot T_{local} \cdot T_{space}$$
Příslušné složené transformace se předpočítávají na straně CPU.

Ve vertex shaderu je každý vrchol transformován váženým součtem všech 19 transformací daných řídících bodů a příslušnou pohledovou a projekční transformací:
$$Projection \cdot View \cdot \sum(T_i \cdot weight_i)$$

\subsection{Interface a funkce}
Aplikace ve své povaze spoléhá na uměleckou způsobilost uživatele. Z tohoto důvodu nenabízí žádnou fyzikální kontrolu nastavení obličejových řídících bodů. Uvěřitelnost výrazu je tedy plně v režii uživatele. Aplikace je defaultně v editačním režimu ve kterém si uživatel patřičně nastaví výraz dle jeho libosti.

Každý řídící bod může být posunut a rotován. Rotace jsou vnitřně reprezentovány pomocí quaternionů, které jsou vhodnější pro interpolaci přechodů obličejových výrazů. V aplikaci se tyto transformace nastavují prostřednictvím GUI. Aktuálně nastavovaná oblast je podbarvena červenou barvou. Mezi jednotlivými řídícími body je možné rovněž přepínat prostřednictvím GUI. Každý řídící bod lze uvést do výchozího nastavení. To stejné platí i pro celý obličej. Uživatel tak nemusí složitě štelovat nepovedený řídící bod/celý obličej zpátky. 

Po nastavení patřičného výrazu se kterým je uživatel spokojen  si může uložit svůj vytvořený výraz do souboru. Nastavení výrazu mu je ponecháno, tedy může ho jen drobně pozměnit a uložit jako další výraz, případně využít již zmíněného resetování. Výrazy  uživatel snadno uchová i po vypnutí aplikace. Při načtení aplikace jsou zároveň načteny do vnitřní paměti všechny uložené výrazy, které se navíc aktualizují rovnou při běhu aplikace (ihned potom co uživatel nějaký nový výraz uloží). 

Uložené výrazy si uživatel může procházet v režimu prohlížení výrazů, kde se výraz mezi jednotlivými obličeji plynule interpoluje a socha tak působí dojmem, že mění výrazy ve tváři. Druhou variantu, jak uložené výrazy zobrazit, nabízí režim "slideshow", který se hodí například pro účely prezentace. Hodí se klidně i jako spořič obrazovky - jelikož nevyžaduje jakoukoliv aktivitu uživatele. Režim sám náhodně generuje, které obličeje zrovna zobrazí a s jakým zpožděním bude příští výraz zobrazovat.   

\section{Závěr}
Byl úspěšně vytvořen nástroj pro simulaci výrazů lidské tváře.

Prostor pro zlepšení nástroje je hned v několika oblastech. V této chvíli je každý vertex ovlivněn všemi řídícími body. To však není nutné, protože většina vrcholů má nenulové váhy k mnohem menšímu počtu bodů. Pokud by se tento přístup optimalizoval, kromě zvýšení výkonu by také došlo k výraznému zjednodušení portu aplikace na jiný model tváře, případně ke zvýšení počtu řídících bodů.

Kromě vnitřní reprezentace řídících bodů by bylo možné do budoucna zjednodušit také nastavování obličeje. Pokud by se vytvořily hranice posunů a rotací příslušných bodů, práce s aplikací by se zpříjemnila a především přiblížila i umělecky netknutým uživatelům.

Šlo by také udělat uživatelské testování, na základě kterého by se zjistilo, zda-li by se uživatelům lépe pracovalo se dvěma režimy (aktuální stav), nebo by chtěli přepínání obličejů a jejich manipulaci sloučit do jednoho režimu. To je nejspíše uživatelsky přívětivější, ale implementačně náročnější - jelikož do nastavování výrazu se při sloučení těchto funkcionalit míchá interpolace výrazů závislá na čase, která běží nepřetržitě a narušuje tak modifikaci dat provedenou uživatelem v GUI aplikace. 

\appendix
\bibliography{zprava}{}
\bibliographystyle{plain}

\appendix
\section{Příloha}
\subsection{Podíl práce}

Petr Brachaczek:
\begin{itemize}
\item zpracování 3D modelu,
\item obličejový rig,
\item načítání modelu,
\item načítání textur,
\item openGl renderer (veškerá obsluha grafické karty a shadery),
\item ovládání řídících bodů pomocí GUI,
\item a příslušné kapitoly ve zprávě, týkající se zmíněných témat.
\end{itemize}

Sebastian Voráč:
\begin{itemize}
\item datová struktura pro práci s obličeji/kostmi
\item ukládání dat obličejů do souboru
\item načítání a zpracování dat obličejů ze souboru
\item režim přepínaní mezi obličeji
\item restart aktuální kosti/obličeje
\item interpolace (přechody) obličejů mezi sebou
\item prezentační režim aplikace
\end{itemize}

\subsection{Odkazy}
\begin{itemize}
\item Git repozitář - gitlab.fel.cvut.cz/brachpet/MMA\_face/
\end{itemize}

\end{document}


