#pragma once
#include "Shader.h"


class ShaderProgram
{
public:
	Shader vertexShader;
	Shader fragmentShader;
	GLuint programID;
public:
	ShaderProgram(const char * vertexShaderFilePath, const char * fragmentShaderFilePath);
	ShaderProgram();
	void link();
	void use();
};