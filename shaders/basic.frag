#version 330 core

uniform sampler2D tex;
uniform int showBone;

out vec4 color;

in vec3 positionInWorldSpace;
in vec2 uvs;

in vec3 selectedBneColor;

void main(){
	color = texture(tex, uvs); //vec4(vahy, 1.0)*0.1f;
	if (showBone == 1){
		color += vec4(selectedBneColor, 1.0) * 0.5f;
	}
}