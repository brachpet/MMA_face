#version 330 core

layout(location = 0) in vec3 posIn;
layout(location = 2) in vec2 uvs_in;

//vahy narigovaneho obliceje
layout(location = 4) in vec4 w_mouth;	//c_chin | r_mouth | c_mouth | l_mouth
layout(location = 5) in vec4 w_leftEye; //l_brow_1 | l_brow_2 | l_brow_3 | l_eye
layout(location = 6) in vec4 w_rightEye;//r_brow_1 | r_brow_2 | r_brow_3 | r_eye
layout(location = 7) in vec4 w_cheeks;	//l_cheek | l_mouth_cheek | r_cheek | r_mouth_cheek
layout(location = 8) in vec3 w_noseBase;//base | l_nose | r_nose

uniform mat4 MVP;
uniform mat4 w_transform[19];
uniform int selectedIndex;

out vec3 positionInWorldSpace;
out vec2 uvs;
out vec3 selectedBneColor;


void main(){

	uvs = uvs_in;

	mat4 boneTransform = w_transform[0] * w_mouth.x;
	boneTransform     += w_transform[1] * w_mouth.y;
	boneTransform     += w_transform[2] * w_mouth.z;
	boneTransform	  += w_transform[3] * w_mouth.w;
	boneTransform	  += w_transform[4] * w_leftEye.x;
	boneTransform	  += w_transform[5] * w_leftEye.y;
	boneTransform	  += w_transform[6] * w_leftEye.z;
	boneTransform	  += w_transform[7] * w_leftEye.w;
	boneTransform	  += w_transform[8] * w_rightEye.x;
	boneTransform	  += w_transform[9] * w_rightEye.y;
	boneTransform	  += w_transform[10] * w_rightEye.z;
	boneTransform	  += w_transform[11] * w_rightEye.w;
	boneTransform	  += w_transform[12] * w_cheeks.x;
	boneTransform	  += w_transform[13] * w_cheeks.y;
	boneTransform	  += w_transform[14] * w_cheeks.z;
	boneTransform	  += w_transform[15] * w_cheeks.w;
	boneTransform	  += w_transform[16] * w_noseBase.x;
	boneTransform	  += w_transform[17] * w_noseBase.y;
	boneTransform	  += w_transform[18] * w_noseBase.z;

	boneTransform	  += mat4(1.0f) * (1.0f - w_mouth.x - w_mouth.y - w_mouth.z - 
						w_leftEye.x - w_leftEye.y - w_leftEye.z - w_leftEye.w -
						w_rightEye.x - w_rightEye.y - w_rightEye.z - w_rightEye.w -
						w_cheeks.x - w_cheeks.y - w_cheeks.z - w_cheeks.w -
						w_noseBase.x - w_noseBase.y - w_noseBase.z
						);
	vec4 PosL = boneTransform * vec4(posIn, 1.0f);
	positionInWorldSpace = PosL.xyz;

	float svetlost = 0.0;
	switch(selectedIndex){
		case 0:
			svetlost = w_mouth.x;
			break;
		case 1:
			svetlost = w_mouth.y;
			break;
		case 2:
			svetlost = w_mouth.z;
			break;
		case 3:
			svetlost = w_mouth.w;
			break;
		case 4:
			svetlost = w_leftEye.x;
			break;
		case 5:
			svetlost = w_leftEye.y;
			break;
		case 6:
			svetlost = w_leftEye.z;
			break;
		case 7:
			svetlost = w_leftEye.w;
			break;
		case 8:
			svetlost = w_rightEye.x;
			break;
		case 9:
			svetlost = w_rightEye.y;
			break;
		case 10:
			svetlost = w_rightEye.z;
			break;
		case 11:
			svetlost = w_rightEye.w;
			break;
		case 12:
			svetlost = w_cheeks.x;
			break;
		case 13:
			svetlost = w_cheeks.y;
			break;
		case 14:
			svetlost = w_cheeks.z;
			break;
		case 15:
			svetlost = w_cheeks.w;
			break;
		case 16:
			svetlost = w_noseBase.x;
			break;
		case 17:
			svetlost = w_noseBase.y;
			break;
		case 18:
			svetlost = w_noseBase.z;
			break;
		default :
			svetlost = 0.0;
	}
	selectedBneColor = vec3(1.0, 0.0, 0.0) * svetlost;

	gl_Position = MVP * PosL;
}