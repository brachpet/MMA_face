#include "Shader.h"
#include <fstream>
#include <iostream>
#include <vector>

using namespace std;

Shader::Shader(GLenum shaderType, const char * shaderFilePath)
{
	shaderID = glCreateShader(shaderType);

	ifstream shaderStream(shaderFilePath, ifstream::in);
	if (shaderStream.is_open()) {
		string line = "";
		while (getline(shaderStream, line))
			shaderCode += "\n" + line;

		shaderStream.close();
	}
	else {
#ifdef _DEBUG
		printf("Could not read shader source file %s \n", shaderFilePath);
#endif // _DEBUG
	}

#ifdef _DEBUG
	printf("creating Shader, id: %d\n", shaderID);
#endif // _DEBUG

}

Shader::Shader()
{
}

Shader::~Shader()
{
	glDeleteShader(shaderID);

#ifdef _DEBUG
	printf("deleting Shader \n");
#endif // _DEBUG

}

void Shader::compile()
{
	char const * sourcePointer = shaderCode.c_str();
	glShaderSource(shaderID, 1, &sourcePointer, NULL);
	glCompileShader(shaderID);

#ifdef _DEBUG
	GLint result = GL_FALSE;
	int infoLogLength;

	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0) {
		vector<char> vertexShaderErrorMessage(infoLogLength + 1);
		glGetShaderInfoLog(shaderID, infoLogLength, NULL, &vertexShaderErrorMessage[0]);
		printf("%s\n", &vertexShaderErrorMessage[0]);
	}
	else {
		printf("shader compiled\n");
	}
#endif // _DEBUG

}

GLuint Shader::getShaderID() const
{
	return shaderID;
}

void Shader::print() const
{
#ifdef _DEBUG
	printf(shaderCode.c_str());
	printf("\n");
#endif // _DEBUG
}
