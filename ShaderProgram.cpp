#include "ShaderProgram.h"
#include <vector>

using namespace std;

ShaderProgram::ShaderProgram(const char * vertexShaderFilePath, const char * fragmentShaderFilePath)
	: vertexShader(Shader(GL_VERTEX_SHADER, vertexShaderFilePath)), fragmentShader(Shader(GL_FRAGMENT_SHADER, fragmentShaderFilePath))
{

	vertexShader.compile();
	fragmentShader.compile();

	programID = glCreateProgram();

#ifdef _DEBUG
	printf("creating ShaderProgram %d\n", programID);
#endif // _DEBUG

}

ShaderProgram::ShaderProgram()
{
}


void ShaderProgram::link()
{
#ifdef _DEBUG
	printf("Linking program \n");
#endif // _DEBUG


	glAttachShader(programID, vertexShader.getShaderID());
	glAttachShader(programID, fragmentShader.getShaderID());
	glLinkProgram(programID);

	//printf("shaderID %d\n", vertexShader.getShaderID());
	//printf("shaderID %d\n", fragmentShader.getShaderID());

#ifdef _DEBUG
	// error check
	GLint result = GL_FALSE;
	int infoLogLength;

	glGetProgramiv(programID, GL_LINK_STATUS, &result);
	glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infoLogLength);
	if (infoLogLength > 0) {
		vector<char> ProgramErrorMessage(infoLogLength + 1);
		glGetProgramInfoLog(programID, infoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}
#endif // _DEBUG

	glDetachShader(programID, vertexShader.getShaderID());
	glDetachShader(programID, fragmentShader.getShaderID());

}

void ShaderProgram::use()
{
	glUseProgram(programID);
}
