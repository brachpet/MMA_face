#pragma once
#include "GL/glew.h"
#include <string>

using namespace std;

class Shader
{
	GLuint shaderID;
	string shaderCode;

public:
	Shader(GLenum shaderType, const char * shaderFilePath);
	Shader();
	~Shader();

	void compile();
	GLuint getShaderID() const;	//const means it will not modify shaderID
	void print() const;
};