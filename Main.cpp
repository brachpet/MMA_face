#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include <AntTweakBar.h>
#include <IL/il.h>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

#include "Face.h"
#include "ShaderProgram.h"

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

int windowWidth = WINDOW_WIDTH;
int windowHeight = WINDOW_HEIGHT;

Face * face;
float g_Rotation[] = { 0.0f, 0.0f, 0.0f, 1.0f };
float g_Translation[] = { 0.0f, 0.0f, 0.0f };
bool faceNeedToBeChangedNext;

bool faceViewMode = false;
bool slideShowMode = false;
bool editMode = true;

//oznaci dalsi ridici bod v poradi
void TW_CALL nextBone(void *clientData)
{
	face->selectNextBone();
	glm::vec4 rot = face->getRotationOfSelectedBone();
	glm::vec3 pos = face->getTransformOfSelectedBone();
	g_Rotation[0] = rot.x;
	g_Rotation[1] = rot.y;
	g_Rotation[2] = rot.z;
	g_Rotation[3] = rot.w;
	g_Translation[0] = pos.x;
	g_Translation[1] = pos.y;
	g_Translation[2] = pos.z;

}

//oznaci predchozi ridici bod v poradi
void TW_CALL prevBone(void *clientData)
{
	face->selectPrevBone();
	glm::vec4 rot = face->getRotationOfSelectedBone();
	glm::vec3 pos = face->getTransformOfSelectedBone();
	g_Rotation[0] = rot.x;
	g_Rotation[1] = rot.y;
	g_Rotation[2] = rot.z;
	g_Rotation[3] = rot.w;
	g_Translation[0] = pos.x;
	g_Translation[1] = pos.y;
	g_Translation[2] = pos.z;
}

// ulozi aktualni oblicej do souboru
void TW_CALL saveActualFace(void *clientData)
{
	face->saveActualFace();
	face->loadFaces("savedfaces.txt");
}

// uvede cely oblicej do vychoziho nastaveni
void TW_CALL resetFace (void *clientData)
{
	face->resetFace();
	glm::vec4 rot = face->getRotationOfSelectedBone();
	glm::vec3 pos = face->getTransformOfSelectedBone();
	g_Rotation[0] = 0.0f;
	g_Rotation[1] = 0.0f;
	g_Rotation[2] = 0.0f;
	g_Rotation[3] = 0.0f;
	g_Translation[0] = 0.0f;
	g_Translation[1] = 0.0f;
	g_Translation[2] = 0.0f;
}

void TW_CALL resetActualBone(void *clientData)
{
	glm::vec4 rot = face->getRotationOfSelectedBone();
	glm::vec3 pos = face->getTransformOfSelectedBone();
	g_Rotation[0] = 0.0f;
	g_Rotation[1] = 0.0f;
	g_Rotation[2] = 0.0f;
	g_Rotation[3] = 0.0f;
	g_Translation[0] = 0.0f;
	g_Translation[1] = 0.0f;
	g_Translation[2] = 0.0f;
}

void nextFace()
{
	faceNeedToBeChangedNext = true;
	int newFaceNumber = face->getFaceNumber();
	face->setFaceNumber(newFaceNumber+1);
	glm::vec4 rot = face->getRotationOfSelectedBone();
	glm::vec3 pos = face->getTransformOfSelectedBone();
	g_Rotation[0] = rot.x;
	g_Rotation[1] = rot.y;
	g_Rotation[2] = rot.z;
	g_Rotation[3] = rot.w;
	g_Translation[0] = pos.x;
	g_Translation[1] = pos.y;
	g_Translation[2] = pos.z;
}

void TW_CALL nextFaceCallback(void *clientData)
{
	nextFace();
}

void previousFace()
{
	faceNeedToBeChangedNext = true;
	int newFaceNumber = face->getFaceNumber();
	face->setFaceNumber(newFaceNumber - 1);
	glm::vec4 rot = face->getRotationOfSelectedBone();
	glm::vec3 pos = face->getTransformOfSelectedBone();
	g_Rotation[0] = rot.x;
	g_Rotation[1] = rot.y;
	g_Rotation[2] = rot.z;
	g_Rotation[3] = rot.w;
	g_Translation[0] = pos.x;
	g_Translation[1] = pos.y;
	g_Translation[2] = pos.z;
}

void TW_CALL previousFaceCallback(void *clientData)
{
	previousFace();
}

void changeSlideshowMode() {
	if (slideShowMode) {
		slideShowMode = false;
		editMode = true;
	}
	else {
		editMode = false;
		faceViewMode = false;
		slideShowMode = true;
	}
}

void changeFaceviewMode() {
	if (faceViewMode) {
		faceViewMode = false;
		editMode = true;
	}
	else {
		editMode = false;
		slideShowMode = false;
		faceViewMode = true;
	}
}

float posY = 0.0f;

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	TwEventMouseButtonGLFW(button, action);
}

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
	TwEventMousePosGLFW(xpos, ypos);
}

int main() {

	GLFWwindow* window;
	if (!glfwInit()) {
		return -1;
	}
	else {
		//std::cout << "uspesna inicializace glfw" << std::endl;
	}

	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_COMPAT_PROFILE, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // We don't want the old OpenGL 

	//vytvoreni okna
	window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "MMA", NULL, NULL);	//windowed

	//primarni monitor
	GLFWmonitor* myMonitor = glfwGetPrimaryMonitor();
	//parametry monitoru
	const GLFWvidmode* mode = glfwGetVideoMode(myMonitor);

	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	glewExperimental = true; // Needed in core profile
	if (glewInit() != GLEW_OK)
		return -1;

	//init pro nacitan textur
	ilInit();

	float previousTime = 0;
	float currentTime = glfwGetTime();
	float currentDeltaTime = 0;

	face = new Face();

	//tweakBar gui init
	TwInit(TW_OPENGL_CORE, NULL);
	TwWindowSize(WINDOW_WIDTH, WINDOW_HEIGHT);

	TwBar *myBar = TwNewBar("Ovladace");

	//nastaveni gui okna
	TwDefine(" 'Ovladace' size='200 310' ");
	TwDefine(" 'Ovladace' color='100 100 100' alpha=128 ");
	TwDefine(" 'Ovladace' resizable=false ");

	TwAddButton(myBar, "Dalsi kost", nextBone, NULL, " label='Dalsi kost' group='Oblicej'");
	TwAddButton(myBar, "Predchozi kost", prevBone, NULL, " label='Predchozi kost' group='Oblicej'");
	TwAddButton(myBar, "Uloz aktualni oblicej", saveActualFace, NULL, " label='Uloz aktualni oblicej' group='Oblicej'");
	TwAddVarRW(myBar, "Rotace", TW_TYPE_QUAT4F, &g_Rotation," group='Ovladac kosti' opened=true help='Change the object orientation.' ");
	TwAddVarRW(myBar, "Posun x", TW_TYPE_FLOAT, &g_Translation[0], " group='Ovladac kosti' min=-0.1 max=0.1 step=0.01 ");
	TwAddVarRW(myBar, "Posun y", TW_TYPE_FLOAT, &g_Translation[1], " group='Ovladac kosti' min=-0.1 max=0.1 step=0.01 ");
	TwAddVarRW(myBar, "Posun z", TW_TYPE_FLOAT, &g_Translation[2], " group='Ovladac kosti' min=-0.1 max=0.1 step=0.01 ");
	TwAddButton(myBar, "Reset aktualni kosti", resetActualBone, NULL, " label='Reset aktualni kosti' group='Resetovani'");
	TwAddButton(myBar, "Reset obliceje", resetFace, NULL, " label='Reset obliceje' group='Resetovani'");

	/*
	TwBar *myBarFaceSwitching = TwNewBar("Prepinani obliceju");
	
	//nastaveni gui okna
	TwDefine(" 'Prepinani obliceju' size='200 100' ");
	TwDefine(" 'Prepinani obliceju' color='100 100 100' alpha=128 ");
	TwDefine(" 'Prepinani obliceju' resizable=false ");

	TwAddButton(myBarFaceSwitching, "Dalsi oblicej", nextFaceCallback, NULL, " label='Dalsi oblicej' group='Oblicej'");
	TwAddButton(myBarFaceSwitching, "Predchozi oblicej", previousFaceCallback, NULL, " label='Predchozi oblicej' group='Oblicej'");
	*/

	//tweakBar gui callback
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetCursorPosCallback(window, cursor_position_callback);

	//app info
	std::cout << "S - zobrazi nebo skryje gui" << std::endl;
	std::cout << "Page Up / Page Down - zapina nebo vypina fullscreen" << std::endl;
	std::cout << "Mezernik - Zapina/Vypina rezim prohlizeni obliceju (sipka doleva / doprava - prepina ulozene obliceje)" << std::endl;
	std::cout << "Enter - Zapina/Vypina automatickou nahodnou slideshow obliceju" << std::endl;
	std::cout << "Poznamka - Oba rezimy mozno zapnout pouze z defaultniho (editacniho) rezimu" << std::endl;

	glEnable(GL_CULL_FACE);
	glClearDepth(1.0f);
	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);

	//kontrolni promenna, ktera hlida uvolneni klavesy, aby se callback zavolal jen jednou
	int skeyChange = GLFW_RELEASE;
	int skeyChangeLeft = GLFW_RELEASE;
	int skeyChangeRight = GLFW_RELEASE;
	int skeyChangeEnter = GLFW_RELEASE;
	int skeyChangeSpace = GLFW_RELEASE;

	bool needReset = false;

	bool showGui = true;

	while (!glfwWindowShouldClose(window))
	{

		previousTime = currentTime;
		currentTime = glfwGetTime();

		float dt = (currentTime - previousTime) / 7;

		currentDeltaTime = currentDeltaTime + dt;

		// lock the value if game freeze and delta would be too big
		if (dt > 0.015f) {
			dt = 0.015f;
		}

		// face changes according to time (for interpolation)
		if (!editMode) {
			face->switchFace(false, dt);
		}
		//ziskani pozice mysi
		double mouseX = 0, mouseY = 0;
		glfwGetCursorPos(window, &mouseX, &mouseY);

		posY = mouseX;

		//rozmery okna
		int width, height;
		glfwGetWindowSize(window, &width, &height);

		windowWidth = width;
		windowHeight = height;

		TwWindowSize(windowWidth, windowHeight);

		glViewport(0, 0, width, height);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glEnable(GL_DOUBLEBUFFER);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		face->render();	//vyrenderuje oblicej
		if (showGui) TwDraw();		//vyrenderuje gui

		/* Swap front and back buffers */
		glfwSwapBuffers(window);

		/* Poll for and process events */
		glfwPollEvents();

		// zavreni okna na klavesu ESC
		if (glfwGetKey(window, GLFW_KEY_ESCAPE)) {
			glfwSetWindowShouldClose(window, 1);
		}

		if (skeyChangeEnter != glfwGetKey(window, GLFW_KEY_ENTER) && !faceViewMode) {
			skeyChangeEnter = glfwGetKey(window, GLFW_KEY_ENTER);
			if (skeyChangeEnter == GLFW_PRESS) {
				changeSlideshowMode();
				showGui = !showGui;
				face->showBone = !face->showBone;
			}
		}

		if (skeyChangeSpace != glfwGetKey(window, GLFW_KEY_SPACE) && !slideShowMode) {
			skeyChangeSpace = glfwGetKey(window, GLFW_KEY_SPACE);
			if (skeyChangeSpace == GLFW_PRESS) {
				changeFaceviewMode();
				showGui = !showGui;
				face->showBone = !face->showBone;
			}
		}

		//toggle fullscreen
		if (glfwGetKey(window, GLFW_KEY_PAGE_UP)) {
			glfwSetWindowMonitor(window, myMonitor, 0, 0, mode->width, mode->height, mode->refreshRate);
		}

		//toggle windowed mode
		if (glfwGetKey(window, GLFW_KEY_PAGE_DOWN)) {
			glfwSetWindowMonitor(window, NULL, 100, 100, WINDOW_WIDTH, WINDOW_HEIGHT, mode->refreshRate);
		}

		if (skeyChangeRight != glfwGetKey(window, GLFW_KEY_RIGHT)) {
			skeyChangeRight = glfwGetKey(window, GLFW_KEY_RIGHT);
			if (skeyChangeRight == GLFW_PRESS) {
				nextFace();
			}
		}

		if (skeyChangeLeft != glfwGetKey(window, GLFW_KEY_LEFT)) {
			skeyChangeLeft = glfwGetKey(window, GLFW_KEY_LEFT);
			if (skeyChangeLeft == GLFW_PRESS) {
				previousFace();
			}
		}

		//oznaci dalsi ridici bod v poradi
		if (skeyChange != glfwGetKey(window, GLFW_KEY_S)) {
			skeyChange = glfwGetKey(window, GLFW_KEY_S);
			if (skeyChange == GLFW_PRESS) {
				showGui = !showGui;
				face->showBone = !face->showBone;
			}
		}

		if (slideShowMode && ((currentDeltaTime + (((double)rand() / RAND_MAX) / 100)) > 0.25)) {
			if (rand() % 2 == 0) {
				needReset = true;
				currentDeltaTime = 0;
				face->switchFace(true, dt);
			}
		}

		//nastavi trasnformaci oznacene kosti podle gui
		if (editMode) {
			face->setTransformOfSelectedBoneFromGui(g_Translation);
			face->setRotationOfSelectedBoneFromGui(g_Rotation);
		}
	}

	glfwTerminate();
	delete face;
}