#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/Importer.hpp>

#include <IL/il.h>

#include <map>
#include <vector>

#include "Face.h"

using namespace std;

extern int windowWidth;
extern int windowHeight;
extern float posY;

Bone* boneInfo;


float translation_reset[] = { 0.0f, 0.0f, 0.0f };
float rotation_reset[] = { 0.0f, 0.0f, 0.0f, 0.0f };

struct boneStruct
{
	float transformX, transformY, transformZ,
		rotationX, rotationY, rotationZ, rotationW;
};

struct faceStruct
{
	// array of pointers
	boneStruct *allFaceBonesArray[19];
};

vector<faceStruct*> vectOfFaces;

///mapuje nazev kosti na index kosti
int boneIndex(const std::string &nazev) {
	if (nazev.compare("c_chin") == 0) return 0;
	if (nazev.compare("r_mouth") == 0) return 1;
	if (nazev.compare("c_mouth") == 0) return 2;
	if (nazev.compare("l_mouth") == 0) return 3;
	if (nazev.compare("l_brow_1") == 0) return 4;
	if (nazev.compare("l_brow_2") == 0) return 5;
	if (nazev.compare("l_brow_3") == 0) return 6;
	if (nazev.compare("l_eye") == 0) return 7;
	if (nazev.compare("r_brow_1") == 0) return 8;
	if (nazev.compare("r_brow_2") == 0) return 9;
	if (nazev.compare("r_brow_3") == 0) return 10;
	if (nazev.compare("r_eye") == 0) return 11;
	if (nazev.compare("l_cheek") == 0) return 12;
	if (nazev.compare("l_mouth_cheek") == 0) return 13;
	if (nazev.compare("r_cheek") == 0) return 14;
	if (nazev.compare("r_mouth_cheek") == 0) return 15;
	if (nazev.compare("base") == 0) return 16;
	if (nazev.compare("l_nose") == 0) return 17;
	if (nazev.compare("r_nose") == 0) return 18;
	return -1;
}

void printMatrix(const aiMatrix4x4 &kost) {
	std::cout << kost.a1 << " " << kost.a2 << " " << kost.a3 << " " << kost.a4 << std::endl;
	std::cout << kost.b1 << " " << kost.b2 << " " << kost.b3 << " " << kost.b4 << std::endl;
	std::cout << kost.c1 << " " << kost.c2 << " " << kost.c3 << " " << kost.c4 << std::endl;
	std::cout << kost.d1 << " " << kost.d2 << " " << kost.d3 << " " << kost.d4 << std::endl << std::endl;
}

glm::mat4 toGlmMatrix(const aiMatrix3x3 &kost) {
	glm::mat4 matrix = glm::mat4(1.0f);

	matrix[0][0] = kost.a1;
	matrix[0][1] = kost.a2;
	matrix[0][2] = kost.a3;

	matrix[1][0] = kost.b1;
	matrix[1][1] = kost.b2;
	matrix[1][2] = kost.b3;

	matrix[2][0] = kost.c1;
	matrix[2][1] = kost.c2;
	matrix[2][2] = kost.c3;

	return matrix;
}

glm::mat4 convertQuaternionToMatrix(const float *quat)
{
	float yy2 = 2.0f * quat[1] * quat[1];
	float xy2 = 2.0f * quat[0] * quat[1];
	float xz2 = 2.0f * quat[0] * quat[2];
	float yz2 = 2.0f * quat[1] * quat[2];
	float zz2 = 2.0f * quat[2] * quat[2];
	float wz2 = 2.0f * quat[3] * quat[2];
	float wy2 = 2.0f * quat[3] * quat[1];
	float wx2 = 2.0f * quat[3] * quat[0];
	float xx2 = 2.0f * quat[0] * quat[0];

	glm::mat4 mat = glm::mat4(0.0f);

	mat[0][0] = -yy2 - zz2 + 1.0f;
	mat[0][1] = xy2 + wz2;
	mat[0][2] = xz2 - wy2;
	mat[0][3] = 0;
	mat[1][0] = xy2 - wz2;
	mat[1][1] = -xx2 - zz2 + 1.0f;
	mat[1][2] = yz2 + wx2;
	mat[1][3] = 0;
	mat[2][0] = xz2 + wy2;
	mat[2][1] = yz2 - wx2;
	mat[2][2] = -xx2 - yy2 + 1.0f;
	mat[2][3] = 0;
	mat[3][0] = mat[3][1] = mat[3][2] = 0;
	mat[3][3] = 1;

	return mat;
}

glm::mat4 toGlmMatrix(const aiMatrix4x4 &kost) {
	glm::mat4 matrix = glm::mat4(1.0f);

	matrix = glm::transpose(glm::make_mat4(&kost.a1));

	return matrix;
}

void Face::loadMesh()
{
	// testovani assimp loaderu
	std::string fileName = "busta.FBX";	// model.FBX objekt.OBJ boblampclean.md5mesh

	Assimp::Importer importer;
	const aiScene * scn = importer.ReadFile(fileName.c_str(), 0
		| aiProcess_Triangulate             // Triangulate polygons (if any).
		| aiProcess_GenSmoothNormals        // Calculate normals per vertex.
		| aiProcess_JoinIdenticalVertices
	);

	if (!scn) {
		std::cerr << "assimp error: " << importer.GetErrorString() << std::endl;
		return;
	}

	if (scn->mNumMeshes == 0) {
		std::cerr << "assimp error: zero number of meshes" << std::endl;
		return;
	}
	//bereme jen jeden mesh
	const aiMesh * mesh = scn->mMeshes[0];

	// cte kosti a jejich matice pro trasnformaci do lokalniho prostoru kosti
	boneInfo = new Bone[mesh->mNumBones];

	for (int i = 0; i < mesh->mNumBones; i++)
	{
		aiBone* kost = mesh->mBones[i];
		//std::cout << kost->mName.C_Str() << std::endl;
		//printMatrix(kost->mOffsetMatrix);

		boneInfo[i].spaceTransform = toGlmMatrix(kost->mOffsetMatrix);
		boneInfo[i].spaceTransformInverse = glm::inverse(boneInfo[i].spaceTransform);
		boneInfo[i].localTransform = glm::mat4(1.0f);
	}
	std::cout << std::endl << std::endl;
	

	// informace o modelu
	assimpStride = 8 + mesh->mNumBones;	// xyz pozice, xyz normala, uv mapovani, + 1 vaha na kazdou z kosti
	assimpMesh = new float[assimpStride * mesh->mNumVertices]; // pozice, normaly uv => (3 + 3 // + 2)
	for (int i = 0; i < assimpStride * mesh->mNumVertices; i++)
	{
		assimpMesh[i] = 0.0f;
	}

	assimpNumTriangles = mesh->mNumFaces;

	for (int i = 0; i < scn->mNumMeshes; i++)
	{
		//std::cout << "pocet kosti pro mesh " << i << ": " << scn->mMeshes[i]->mNumBones << std::endl;
	}

	//prekopirovani dat meshe do naseho pole
	for (int i = 0; i < mesh->mNumVertices; i++)
	{
		//pozice
		assimpMesh[assimpStride * i + 0] = mesh->mVertices[i].x;
		assimpMesh[assimpStride * i + 1] = mesh->mVertices[i].y;
		assimpMesh[assimpStride * i + 2] = mesh->mVertices[i].z;

		//normala
		assimpMesh[assimpStride * i + 3] = mesh->mNormals[i].x;
		assimpMesh[assimpStride * i + 4] = mesh->mNormals[i].y;
		assimpMesh[assimpStride * i + 5] = mesh->mNormals[i].z;

		//uv
		assimpMesh[assimpStride * i + 6] = mesh->mTextureCoords[0][i].x;
		assimpMesh[assimpStride * i + 7] = mesh->mTextureCoords[0][i].y;
	}

	//nacteni vah z kosti, musim to udelat tak, protoze ne kazda kost si nese informaci o vsech vertexech (nemam informaci o tech vertexech, kde je vaha nulova), proto ji musim zapsat pomoci mVertexId na spravne misto v bufferu
	for (int i = 0; i < mesh->mNumBones; i++)
	{
		aiBone* kost = mesh->mBones[i];
		const char * nazevKosti = kost->mName.C_Str();
		std::string nazev = nazevKosti;

		int weightIndex = boneIndex(nazev);

		for (int j = 0; j < mesh->mBones[i]->mNumWeights; j++)
		{
			assimpMesh[assimpStride * mesh->mBones[i]->mWeights[j].mVertexId + 8 + weightIndex] = mesh->mBones[i]->mWeights[j].mWeight;	//pozice v bufferu je posunuta o assimp stride a pred nim je 8 dalsich hodnot (pozice, normala, uv) + ukladam na i-tou pozici
		}
	}

	glGenVertexArrays(1, &assimpVertexArrayID);	//uklada setup attribut pointeru
	glBindVertexArray(assimpVertexArrayID);

	// zabali kompletni balik vertexu s pozicemi, normalami a tak dale do bufferu
	glGenBuffers(1, &assimpVertexBufferObject);
	glBindBuffer(GL_ARRAY_BUFFER, assimpVertexBufferObject);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * mesh->mNumVertices * assimpStride, assimpMesh, GL_STATIC_DRAW); //buffer ponese pozice (3x float), normaly (3x float) //, uv (2x float)

	//specifikovani attribut pointeru pro pozici a mapovani textur
	GLuint position = glGetAttribLocation(shaderProgram.programID, "posIn");
	glEnableVertexAttribArray(position);
	glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, assimpStride * sizeof(float), 0);
	GLuint uvs = glGetAttribLocation(shaderProgram.programID, "uvs_in");
	glEnableVertexAttribArray(uvs);
	glVertexAttribPointer(uvs, 2, GL_FLOAT, GL_FALSE, assimpStride * sizeof(float), (void*)(6 * sizeof(float)));

	//specifikovani vah kosti do jednotliych vstupnich atributu
	GLuint weight = glGetAttribLocation(shaderProgram.programID, "w_mouth");
	glEnableVertexAttribArray(weight);
	glVertexAttribPointer(weight, 4, GL_FLOAT, GL_FALSE, assimpStride * sizeof(float), (void*)(8 * sizeof(float)));
	weight = glGetAttribLocation(shaderProgram.programID, "w_leftEye");
	glEnableVertexAttribArray(weight);
	glVertexAttribPointer(weight, 4, GL_FLOAT, GL_FALSE, assimpStride * sizeof(float), (void*)((8 + 4)* sizeof(float)));
	weight = glGetAttribLocation(shaderProgram.programID, "w_rightEye");
	glEnableVertexAttribArray(weight);
	glVertexAttribPointer(weight, 4, GL_FLOAT, GL_FALSE, assimpStride * sizeof(float), (void*)((8 + 8) * sizeof(float)));
	weight = glGetAttribLocation(shaderProgram.programID, "w_cheeks");
	glEnableVertexAttribArray(weight);
	glVertexAttribPointer(weight, 4, GL_FLOAT, GL_FALSE, assimpStride * sizeof(float), (void*)((8 + 12) * sizeof(float)));
	weight = glGetAttribLocation(shaderProgram.programID, "w_noseBase");
	glEnableVertexAttribArray(weight);
	glVertexAttribPointer(weight, 3, GL_FLOAT, GL_FALSE, assimpStride * sizeof(float), (void*)((8 + 16) * sizeof(float)));

	// vytvoreni indexoveho bufferu. Na kazdy trojuhelnik potrebujeme celkem 3 indexy (pro kazdy vrchol jeden)
	unsigned * indices = new unsigned[mesh->mNumFaces * 3];
	for (unsigned f = 0; f < mesh->mNumFaces; ++f) {
		indices[f * 3 + 0] = mesh->mFaces[f].mIndices[0];
		indices[f * 3 + 1] = mesh->mFaces[f].mIndices[1];
		indices[f * 3 + 2] = mesh->mFaces[f].mIndices[2];
	}

	// prekopirovani do element bufferu
	glGenBuffers(1, &assimpElementArrayObject);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, assimpElementArrayObject);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 3 * sizeof(unsigned) * mesh->mNumFaces, indices, GL_STATIC_DRAW);
	delete[] indices;

	
	//odpojeni nasich bufferu, attributPointer setup je ulozen v assimpVertexArrayID
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);


	//transformace kosti
	boneTransformations = new glm::mat4[mesh->mNumBones];
	for (int i = 0; i < mesh->mNumBones; i++)
	{
		boneTransformations[i] = glm::mat4(1.0f);
	}

}

void Face::loadTextures()
{
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);
	// set linear filtering
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);	//chci generovat mipmapy
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	ILuint img_id;
	ilGenImages(1, &img_id); // generate one image ID (name)
	ilBindImage(img_id); // bind that generated id

						 // set origin to LOWER LEFT corner (the orientation which OpenGL uses)
	ilEnable(IL_ORIGIN_SET);
	ilSetInteger(IL_ORIGIN_MODE, IL_ORIGIN_LOWER_LEFT);

	std::string textureLocation = "diffuse.jpg"; //"masaryk_diffuse.jpg";

	// this will load image data to the currently bound image
	if (ilLoadImage(textureLocation.c_str()) == IL_FALSE) {
		ilDeleteImages(1, &img_id);
		std::cout << __FUNCTION__ << " cannot load image " << textureLocation << std::endl;
		//return;
	}

	// if the image was correctly loaded, we can obtain some informatins about our image
	ILint width = ilGetInteger(IL_IMAGE_WIDTH);
	ILint height = ilGetInteger(IL_IMAGE_HEIGHT);
	ILenum format = ilGetInteger(IL_IMAGE_FORMAT);
	// there are many possible image formats and data types
	// we will convert all image types to RGB or RGBA format, with one byte per channel
	unsigned Bpp = ((format == IL_RGBA || format == IL_BGRA) ? 4 : 3);
	char * data = new char[width * height * Bpp];
	// this will convert image to RGB or RGBA, one byte per channel and store data to our array
	ilCopyPixels(0, 0, 0, width, height, 1, Bpp == 4 ? IL_RGBA : IL_RGB, IL_UNSIGNED_BYTE, data);
	// image data has been copied, we dont need the DevIL object anymore
	ilDeleteImages(1, &img_id);

	// bogus ATI drivers may require this call to work with mipmaps
	glEnable(GL_TEXTURE_2D);
	glTexImage2D(GL_TEXTURE_2D, 0, Bpp == 4 ? GL_RGBA : GL_RGB, width, height, 0, Bpp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data);

	// free our data (they were copied to OpenGL)
	delete[] data;

	glGenerateMipmap(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);
}


void Face::loadFaces(string filename)
{

	fstream facesfileToLoad;
	string line;
	facesfileToLoad.open(filename);
	int numOfactualBones = 0;
	int numOfFaces = 0;
	int i = 1;
	int trans = 0;
	int rots = 0;
	float transArrayTemp[3];
	float rotArrayTemp[4];

	boneStruct* actualBone = new boneStruct; 
	faceStruct* actualFace = new faceStruct;

	if (facesfileToLoad.is_open() && facesfileToLoad)
	{
		while (getline(facesfileToLoad, line))  // stejne jako: while (getline( myfile, line ).good())
		{
			istringstream is(line);
			float xTran ,yTran, zTran, xRot, yRot, zRot, wRot;

			if (i % 2 == 1) {
				is >> actualBone->transformX >> actualBone->transformY >> actualBone->transformZ;
			}
			else {
				is >> actualBone->rotationX >> actualBone->rotationY >> actualBone->rotationZ >> actualBone->rotationW;
			}
			i++;
			if (i == 3) { // save the bone data

				actualFace->allFaceBonesArray[numOfactualBones] = actualBone;
				actualBone = new boneStruct;
				numOfactualBones++;
				i = 1;
			}
			if (numOfactualBones == 19) { // save the face data
				vectOfFaces.push_back(actualFace);
				actualFace = new faceStruct;
				numOfFaces++;
				numberOfTotalFaces++;
				numOfactualBones = 0;
			}
		}
	}
	facesfileToLoad.close();
}

Face::Face() : shaderProgram(ShaderProgram("shaders/basic.vert", "shaders/basic.frag"))
{
	//pripraveni shader programu
	shaderProgram.link();
	t = 0.0f;

	// nahraje model hlavy a vytvori potrebne openGL buffery
	loadMesh();

	//lokace vsech transformacnich matic
	for (int i = 0; i < 19; i++)
	{
		std::string location = std::string("w_transform[") + std::to_string(i) + std::string("]");
		w_transformLocation[i] = glGetUniformLocation(shaderProgram.programID, location.c_str());
	}

	// nahraje potrebne textury
	loadTextures();

	// nahraje tvare ulozene ze souboru
	loadFaces("defaultfaces.txt");
	loadFaces("savedfaces.txt");

}


Face::~Face()
{
	delete[] vertexData;
	delete[] assimpMesh;
}

void Face::render()
{
	//zacit pouzivat shader program
	shaderProgram.use();

	glm::mat4 model = glm::mat4(1.0f);
	glm::mat4 view = glm::lookAt(
		glm::vec3((960.0f - posY) / 1000.0f , -2.5f, 0.7f), // the position of your camera, in world space //
		glm::vec3(0.0f, 0.0f, 0.5f),   // where you want to look at, in world space
		glm::vec3(0, 0, 1)        // probably glm::vec3(0,1,0), but (0,-1,0) would make you looking upside-down, which can be great too
	);

	glm::mat4 projection = glm::perspective(
		glm::radians(45.0f), (float)windowWidth / (float)windowHeight, 0.1f, 300.0f);


	glm::mat4 MVP = projection * view * model;

	GLuint mvpLocation = glGetUniformLocation(shaderProgram.programID, "MVP");
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, &MVP[0][0]);

	GLuint textureLocation = glGetUniformLocation(shaderProgram.programID, "tex");
	glUniform1i(textureLocation, 0);

	GLuint selectedBoneLocation = glGetUniformLocation(shaderProgram.programID, "selectedIndex");
	glUniform1i(selectedBoneLocation, selectedBone);

	GLuint showBoneLoc = glGetUniformLocation(shaderProgram.programID, "showBone");
	glUniform1i(showBoneLoc, showBone);
	

	//vypocet transformacnich matic ridicich bodu
	for (int i = 0; i < 19; i++)
	{
		glm::mat4 kostTransform = boneInfo[i].spaceTransformInverse * boneInfo[i].getLocalTransform() * boneInfo[i].spaceTransform;
		boneTransformations[i] = kostTransform;
	}

	//pripojeni tranformacnich matic do shaderu
	for (int i = 0; i < 19; i++)
	{
		glUniformMatrix4fv(w_transformLocation[i], 1, GL_FALSE, &boneTransformations[i][0][0]);
	}

	glBindTexture(GL_TEXTURE_2D, tex);

	glBindVertexArray(assimpVertexArrayID);	//nacte objekt, ktery obsahuje setup vertex bufferu a attribut pointeru
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, assimpElementArrayObject); // bind our element array buffer (indices) to vao

	//glDrawArrays(GL_TRIANGLES, 0, assimpNumTriangles*3);	//bude fungovat taky, protoze vertexy nejsou optimalizovane, takze nepotrebuju indexy, ale jen u OBJ formatu
	glDrawElements(GL_TRIANGLES, assimpNumTriangles * 3, GL_UNSIGNED_INT, 0);

	glBindVertexArray(0);
}

void Face::selectBone(int index)
{
	selectedBone = index % 19;
}

void Face::selectNextBone()
{
	selectedBone++;
	selectedBone %= 19;
}

void Face::selectPrevBone()
{
	selectedBone--;
	selectedBone += 19;
	selectedBone %= 19;
}

void Face::saveActualFace()
{
	fstream facesfile;
	string line;
	facesfile.open("savedfaces.txt", std::ios_base::app);
	int numOfFaces;
	if (facesfile.is_open())
	{
		int prviouslySelectedBone = selectedBone;
		// loop trought all bones and save their transformation and rotation data
		for (int i = 0; i < 19; i++)
		{
			selectedBone = i;
			glm::vec3 actualTransformBoneInfo = getTransformOfSelectedBone();
			glm::vec4 actualRotationBoneInfo = getRotationOfSelectedBone();

			facesfile << actualTransformBoneInfo.x << " " << actualTransformBoneInfo.y << " " << actualTransformBoneInfo.z << endl;
			facesfile << actualRotationBoneInfo.x << " " << actualRotationBoneInfo.y << " " << actualRotationBoneInfo.z << " " << actualRotationBoneInfo.w << endl;
		}
		selectedBone = prviouslySelectedBone; // go back to bone user was before he did the saving
	}
	facesfile.close();
}


void Face::resetFace()
{
	int actualBone = selectedBone;
	for (int i = 0; i < 19; i++)
	{
		selectedBone = i;
		resetTransform(translation_reset);
		resetRotation(rotation_reset);
	}
	selectedBone = actualBone;
}

void Face::switchFace(bool random, float dt) // true for next , false for previous
{
	int actualBone = selectedBone;

	if (random) {
		int faceNumRand = rand() % numberOfTotalFaces;
		faceNumber = faceNumRand;
	} 

	if (vectOfFaces.size() > 0)
	{
		for (int i = 0; i < 19; i++) // change all bones
		{
			selectedBone = i;
			/* previous bone data - probably no needed, because it can be still accessed like actual data
			boneStruct *previousBone = vectOfFaces[(faceNumber - 1) % vectOfFaces.size()]->allFaceBonesArray[i];
			float previoustranslation_array[] = { previousBone->transformX, previousBone->transformY, previousBone->transformZ, };
			float previousRotation_array[] = { previousBone->rotationX, previousBone->rotationY, previousBone->rotationZ, previousBone->rotationW };
			*/

			boneStruct *actualBone = vectOfFaces[faceNumber % vectOfFaces.size()]->allFaceBonesArray[i];
			float translation_array[] = { actualBone->transformX, actualBone->transformY, actualBone->transformZ, };
			float rotation_array[] = { actualBone->rotationX, actualBone->rotationY, actualBone->rotationZ, actualBone->rotationW };
			setTransformOfSelectedBone(translation_array, dt);
			setRotationOfSelectedBone(rotation_array, dt);
		}
	}
	selectedBone = actualBone;
}

void Face::setFaceNumber(int number)
{
	faceNumber = number;
}

int Face::getFaceNumber()
{
	return faceNumber;
}

void Face::setTransformOfSelectedBone(float * translationGoal, float dt)
{
	float x = approach(translationGoal[0], boneInfo[selectedBone].position.x, dt);
	float y = approach(translationGoal[1], boneInfo[selectedBone].position.y, dt);
	float z = approach(translationGoal[2], boneInfo[selectedBone].position.z, dt);

	boneInfo[selectedBone].localTransform = glm::translate(glm::mat4(1.0f), glm::vec3(x, y, z));
	boneInfo[selectedBone].position.x = x;
	boneInfo[selectedBone].position.y = y;
	boneInfo[selectedBone].position.z = z;
}

void Face::setRotationOfSelectedBone(float * quatGoal, float dt)
{
	float x = approach(quatGoal[0], boneInfo[selectedBone].rotation.x, dt);
	float y = approach(quatGoal[1], boneInfo[selectedBone].rotation.y, dt);
	float z = approach(quatGoal[2], boneInfo[selectedBone].rotation.z, dt);
	float w = approach(quatGoal[3], boneInfo[selectedBone].rotation.w, dt);

	float quatGoalarray[] = { x, y, z, w };

	boneInfo[selectedBone].rotationMat = convertQuaternionToMatrix(quatGoalarray);
	boneInfo[selectedBone].rotation.x = x;
	boneInfo[selectedBone].rotation.y = y;
	boneInfo[selectedBone].rotation.z = z;
	boneInfo[selectedBone].rotation.w = w;
}

void Face::setTransformOfSelectedBoneFromGui(float * translationGoal)
{
	float x = translationGoal[0];
	float y = translationGoal[1];
	float z = translationGoal[2];

	boneInfo[selectedBone].localTransform = glm::translate(glm::mat4(1.0f), glm::vec3(x, y, z));
	boneInfo[selectedBone].position.x = x;
	boneInfo[selectedBone].position.y = y;
	boneInfo[selectedBone].position.z = z;
}

void Face::setRotationOfSelectedBoneFromGui(float * quatGoal)
{
	float x = quatGoal[0];
	float y = quatGoal[1];
	float z = quatGoal[2];
	float w = quatGoal[3];

	boneInfo[selectedBone].rotationMat = convertQuaternionToMatrix(quatGoal);
	boneInfo[selectedBone].rotation.x = x;
	boneInfo[selectedBone].rotation.y = y;
	boneInfo[selectedBone].rotation.z = z;
	boneInfo[selectedBone].rotation.w = w;
}

void Face::resetTransform(float * translation)
{
	boneInfo[selectedBone].localTransform = glm::mat4(1.0f);
	boneInfo[selectedBone].position.x = translation[0];
	boneInfo[selectedBone].position.y = translation[1];
	boneInfo[selectedBone].position.z = translation[2];
}

void Face::resetRotation(float * quat)
{
	boneInfo[selectedBone].rotationMat = glm::mat4(1.0f);
	boneInfo[selectedBone].rotation.x = quat[0];
	boneInfo[selectedBone].rotation.y = quat[1];
	boneInfo[selectedBone].rotation.z = quat[2];
	boneInfo[selectedBone].rotation.w = quat[3];
}

glm::vec3 Face::getTransformOfSelectedBone()
{
	return boneInfo[selectedBone].position;
}

glm::vec4 Face::getRotationOfSelectedBone()
{
	return boneInfo[selectedBone].rotation;
}

// because if we return actual value at the beg. of interpolation, it will not be proper value to set in GUI
glm::vec3 Face::getTransformOfSelectedBoneGoal()
{
	boneStruct *actualBone = vectOfFaces[faceNumber % vectOfFaces.size()]->allFaceBonesArray[selectedBone];
	glm::vec3 translation_array = { actualBone->transformX, actualBone->transformY, actualBone->transformZ, };
	
	return translation_array;
}

// because if we return actual value at the beg. of interpolation, it will not be proper value to set in GUI
glm::vec4 Face::getRotationOfSelectedBoneGoal()
{
	boneStruct *actualBone = vectOfFaces[faceNumber % vectOfFaces.size()]->allFaceBonesArray[selectedBone];
	glm::vec4 rotation_array = { actualBone->rotationX, actualBone->rotationY, actualBone->rotationZ, actualBone->rotationW };

	return rotation_array;
}

glm::mat4 Bone::getLocalTransform()
{
	glm::mat4 transMat = glm::translate(glm::mat4(1.0f), position);
	return rotationMat * transMat;
}

void Bone::setPosition(glm::vec3 pos)
{
	this->position = pos;
}

void Bone::setRotation(glm::mat4 rot)
{
	this->rotationMat = rot;
}

float Face::approach(float goal, float current, float dt)
{
	float diff = goal - current;

	if (diff > dt) {
		return current + dt;
	}
	if (diff < -dt) {
		return current - dt;
	}
	return goal;
}

